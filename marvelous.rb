#!/usr/bin/env ruby
#
# A command line application to facilitate interaction with the Marvel API.
#
# @author  Mark Eissler
#
# Description:
# review the code below that uses the Marvel API (http://developer.marvel.com/).
# You may have to signup for a free account to test
# Please keep this code private (do not post on a public github)

# Tasks:
# - Refactor. Make something worth sharing.
# - Find the total number of characters in Marvel catalog
# - Grab a character from the list and display their thumbnail url
# - List just the character names in the comics id of 30090 and 162
# - Return updated code and steps to generate answers in email attachment.

# ScopeMode (search by string):
#   marvelous --character name ...
#   marvelous --comic title ...
#   marvelous --creator name ...
#
# ScopeMode (search by id):
#   marvelous --character [ --comics | --storys ] id ...
#   marvelous --comic [ --characters | --creators | --storys ] id ...
#   marvelous --creator [ --characters | --comics | --storys ] id ...
#   marvelous --story [ --characters | --comics | --creators ] id ...
#
# Broad mode:
#   marvel_search [--characters | --comics | --creators | --storys ] [--limit number]
#
# options:
# --character <one or more ids or names>
# --comic <one or more ids or titles>
# --creator <one or more ids or names>
# --story <one or more ids>
# --characters
# --comics
# --creators
# --storys
# -l, --limit number
# -o, --offset number
# -s, --short
# --public-key value
# --private-key value
# -d, --debug
# -h, --help
#
# Scope modes - retrieve results in the context of one or more "scope" records
# specified by id in ARGV. Optionally, one or more broad modes can be used to
# retrieve objects within each single "scope" record.
#
# Only ZERO or ONE scope modes can be specified.
#
# Broad modes - retrieve lists of objects within data retrieved from scope.
#
require_relative('./lib/application.rb')

begin
  app = Marvelous::Application.new(ARGV)
  app.run
rescue StandardError => err
  app.handle_error(err.message)
  raise err
end
