# Marvelous CLI

Marvelous CLI is a command line tool for interacting with the Marvel Comics API.

## Features

You can retrieve information about __comics__, __characters__, __creators__, and __stories__ from all data currently available.

## Setup

This is a ruby app, so you will need to have a recent version of ruby installed (the app was developed with ruby 2.3). Outside of ruby, you will need to setup your developer account on Marvel:

[Marvel Developer Site](http://developer.marvel.com/)

With both your public and private keys in hand, you are ready to go. You can provide your keys on the command line each time you use the app (via the `--public-key` and `--private-key` options) or you can provide both keys in a `marvel_keys.json` file:

```
{
  "public_key": "ABC",
  "private_key": "ABC1234"
}
```

Replace the `ABC` and `ABC1234` with your real keys, of course.

To provide keys on the command line:

```
	>marvelous.rb --characters --public-key ABC --private--key ABC1234
```

## Results

By default, results returned are limited to one record. You can request more records with the `--limit` option. The maximum number of records that the API will return at a given time is 100.

## Options

The following options are available and can be retrieved using the `--help` option.

```
Usage: marvelous --ScopeMode [ --BroadMode, ... ] id ...
       marvelous --BroadMode [ --limit number ]

ScopeMode (search by string):
  marvelous --character name ...
  marvelous --comic title ...
  marvelous --creator name ...

ScopeMode (search by id):
  marvelous --character [ --comics | --storys ] id ...
  marvelous --comic [ --characters | --creators | --storys ] id ...
  marvelous --creator [ --characters | --comics | --storys ] id ...
  marvelous --story [ --characters | --comics | --creators ] id ...

BroadMode:
  marvelous [--characters | --comics | --creators | --storys ] [--limit number]

In ScopeMode, when targetting by name or title, a BroadMode cannot be
specified as well. This is a limitation in the Marvel API. Name and
title searches are always perfomed as a "starts with" pattern.

In BroadMode, a maximum of 100 records will be returned at a time.

Parameters:
        --character                  Scope results to character(s) with id(s)
        --comic                      Scope results to comic(s) with id(s)
        --creator                    Scope results to creator(s) with id(s)
        --story                      Scope results to story(s) with id(s)
        --characters                 List characters
        --comics                     List comics
        --creators                   List creators
        --storys                     List stories
    -s, --short                      Output single line results
    -l, --limit number               Limit results to number
    -o, --offset number              Begin ouput from offset
        --public-key string          API public key string
        --private-key string         API private key string
    -d, --debug                      Enable debugging
    -h, --help
```

## Testing Examples

Testing examples with output are provided below. It is assumed that keys have been configured in the `marvel_keys.json` file.

##### Find the total number of characters in Marvel catalog
```
	>marvelous.rb --characters
	Available: 1485
	[   1011334] 3-D Man
```

There are 1485 characters available. The first result is __3-D Man__, which has a character ID of __1011334__.

##### Grab a character from the list and display their thumbnail url

Let's grab a list of 10 characters, starting from 1050 records in. Then, we'll select a single character ID and grab information that includes their thumbnail url (if they have one):

```
	>marvelous.rb --characters --offset 1050 --limit 10
	Available: 1485
	[   1011398] Romulus
	[   1010344] Ronan
	[   1011117] Roughhouse
	[   1009549] Roulette
	[   1009595] Roxanne Simpson
	[   1009311] Rumiko Fujikawa
	[   1010747] Runaways
	[   1009551] Russian
	[   1009552] S.H.I.E.L.D.
	[   1009553] Sabra
	
	>marvelous.rb --character 1009553
	[   1009553] Character name: Sabra
	             Comics: 14              Series: 9              Stories: 12
	             -------------------------------------------------------------------
	             Biography:
	             Ruth Bat-Seraph is an Israeli national who, along with her family,
	             was taken to a special community by the Israeli government when her
	             genetic mutation manifested.
	             -------------------------------------------------------------------
	             Resource : http://marvel.com/characters/1972/sabra
	             Thumbnail: http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg
```


>Note: It seems that most character records do not include biography information. Don't be surprised if you can't find a record with a biography!

It's worth mentioning that you can also search for a character by name:

```
	>marvelous.rb --character Sabra --short
	[   1009553] Character name: Sabra
```
In the above <u>Search By Name</u> example we've also specified the `--short` option to provide single line truncated results. That feature is handy when you return multiple records of information.

##### List just the character names in the comics id of 30090 and 162

You can list character names in a comic by combining both ScopeMode and BroadMode options:

```
	>marvelous.rb --comic 30090 --characters -l 20
	[   1009220] Captain America
	[   1009223] Captain Britain
	[   1009368] Iron Man
	[   1009610] Spider-Man
	[   1009664] Thor
	
	>marvelous.rb --comic 162 --characters -l 20
	[   1009664] Thor
```

The *Scope* is the narrower of the two searches; above we are restricting *Scope* to a particular comic and then requesting a *Broad* search for characters.

>Note: Be sure to provide a `--limit` option otherwise you'll just get a single result back at the most!

You could also specify multiple comics <u>but there is currently a regression bug here that will return incorrect results</u>.

## Some Tricks

You can also return multiple result sets at a time. For instance, the following command will search for characters whose names begin with *Captain America* or *Spider* and then return truncated (short) record results for each, with a limit of 2 records per name:

```
	>marvelous.rb --character "Captain America"  "Spider" -l 2 -s
	[   1009220] Character name: Captain America
	[   1010914] Character name: Captain America (House of M)
	[   1010727] Character name: Spider-dok
	[   1009157] Character name: Spider-Girl (Anya Corazon)
``` 

## Supporting Projects

To exercise the API to facilitate development I used the excellent [Paw HTTP Client for Mac](https://luckymarmot.com/paw) using the Marvel server authorization mechanism. This required the construction of a Paw extension to dynamically generate authorization keys.

The [Marvel API Auth Generator (PAW Extension)]() is available on GitHub:

[https://github.com/markeissler/Paw-MarvelAPIAuth](https://github.com/markeissler/Paw-MarvelAPIAuth)

Enjoy!