#
# lib/marvel_api.rb
#
# @author Mark Eissler
#
module Marvelous
  require 'digest/md5'
  require 'net/http'
  require 'uri'
  require 'date'

  #
  # A class that encapsulates all marvel api functionality to support the
  # main application.
  #
  # @author Mark Eissler
  #
  class MarvelAPI
    attr_reader :public_key, :private_key

    def initialize(options={public_key: '123', private_key: 'ABCD123'})
      @public_key = options[:public_key]
      @private_key = options[:private_key]
    end

    def get_characters(limit=1, id="", filter={})
      params = filter_params(filter)
      endpoint = "characters"
      endpoint << "/#{id}" unless id.empty? or params.length > 0
      api_query(endpoint, params, limit)
    end

    def get_comics(limit=1, id="", filter={})
      params = filter_params(filter)
      endpoint = "comics"
      endpoint << "/#{id}" unless id.empty? or params.length > 0
      api_query(endpoint, params, limit)
    end

    def get_creators(limit=1, id="", filter={})
      params = filter_params(filter)
      endpoint = "creators"
      endpoint << "/#{id}" unless id.empty? or params.length > 0
      api_query(endpoint, params, limit)
    end

    def get_stories(limit=1, id="", filter={})
      params = filter_params(filter)
      endpoint = "stories"
      endpoint << "/#{id}" unless id.empty? or params.length > 0
      result = api_query(endpoint, params, limit)
      # title is returning a description, so grab title from originalIssue
      result['data']['results'].each do |result|
        result['title'] = result['originalIssue']['name']
      end
      return result
    end

    # def people_in_comics(comic_ids_array = [])
    #   comic_ids = to_csv(comic_ids_array)
    #   uri = URI.parse("http://gateway.marvel.com")
    #   http = Net::HTTP.new(uri.host, uri.port)

    #   begin
    #     request = Net::HTTP::Get.new("/v1/public/characters#{auth_params}&limit=1&comics=#{comic_ids}")
    #     @response = http.request(request)
    #     JSON.parse(@response.body)

    #   rescue
    #     # nil
    #   end
    # end

    def self.path_for_image(path_hash)
      path = "unavailable"
      unless path_hash.nil?
        if path_hash.has_key? 'path' and path_hash.has_key? 'extension'
          path = [path_hash['path'], path_hash['extension']].join('.')
        end
      end
      return path
    end

    def self.path_for_url(path_list, type='detail')
      path = "unavailable"
      unless path_list.nil?
        path_list.each do |hash|
          if hash.has_key? 'type' and hash.has_key? 'url' and hash['type'].eql? type
            path = hash['url'].split('?').first
          end
        end
      end
      return path
    end

    def self.text_for_comic_date(date_list, type='onsale')
      type << "Date" # onsaleDate, focDate, unlimitedDate
      text = "unavailable"
      unless date_list.nil?
        date_list.each do |hash|
          if hash.has_key? 'type' and hash.has_key? 'date' and hash['type'].eql? type
            date = DateTime.parse hash['date']
            text = date.strftime('%Y-%m-%d')
          end
        end
      end
      return text
    end

    private
      def api_query(endpoint, params, limit)
        uri_obj = URI.parse("http://gateway.marvel.com")
        http_obj = Net::HTTP.new(uri_obj.host, uri_obj.port)

        url = "/v1/public"
        url << "/#{endpoint}"
        url << "#{auth_params(public_key, private_key)}"
        url << "&limit=#{limit}"
        url << params unless params.empty?

        begin
          response = http_obj.request(Net::HTTP::Get.new(url))
          if [401,403,405].include? response.code.to_i
            raise StandardError, JSON.parse(response.body)['message']
          elsif [404,409].include? response.code.to_i
            raise StandardError, JSON.parse(response.body)['status']
          end
          json_resp = JSON.parse(response.body)
          json_resp ||= {}
        rescue  SocketError, Timeout::Error,
                Errno::EINVAL,
                Errno::ECONNRESET,
                EOFError,
                Net::HTTPBadResponse,
                Net::HTTPHeaderSyntaxError,
                Net::HTTPUnauthorized,
                Net::ProtocolError => err
          raise StandardError, err
        end

        return json_resp
      end

      def auth_params(public_key, private_key)
        ts = Time.now.to_i
        hash = Digest::MD5.hexdigest("#{ts}#{private_key}#{public_key}")
        "?ts=#{ts}&apikey=#{public_key}&hash=#{hash}"
      end

      def filter_params(filter={})
        params = ""
        filter.keys.each do |key|
          unless filter[key].nil? or (filter[key].respond_to?(:empty?) && filter[key].empty?)
            params << "&#{key.to_s}="
            if filter[key].respond_to?(:each)
              params << filter[key].map { |param| URI.escape(param) }.join(',')
            else
              params << URI.escape(filter[key].to_s)
            end
          end
        end

        return params
      end

      def to_csv(array)
        array.join(',')
      end

  end
end
