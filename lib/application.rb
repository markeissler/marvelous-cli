#
# lib/application.rb
#
# @author Mark Eissler
#
module Marvelous
  require_relative "./marvel_api"
  require "json"
  require "optparse"

  #
  # The main application class.
  #
  class Application
    attr_reader :options, :targets, :option_parser

    def initialize(argv)
      Process.setproctitle("#{self.class} init...")
      config = load_config('../lib/version.json')
      @@appname = config[:appname]
      @@version = config[:version]
      Process.setproctitle(@@appname)

      #
      # rescue from invalid key configuration file
      #
      begin
        @options, @targets = parse_options(argv)
      rescue ArgumentError => err
        handle_error(err, true)
        abort
      end

      # debug
      if @options.has_key? :debug_flag
        puts "options are #{@options.inspect}"
        puts "targets are #{@targets.inspect}"
      end
    end

    def handle_error(error, show_help=false)
      puts "#{@@appname}: #{error.message}"
      unless show_help == false
        puts option_parser
      end
      abort
    end

    def run
      marvel_api = Marvelous::MarvelAPI.new({ public_key: options[:public_key], private_key: @options[:private_key] })

      case options[:scope_mode]
      when :character
        begin
          targets.each do |target|
            filter = {}
            unless target.to_i > 0
              filter[:nameStartsWith] = target
            end
            json_resp = marvel_api.get_characters(options[:limit_number], target, filter)
            if options.has_key? :debug_flag
              puts JSON.pretty_generate(json_resp)
            end
            json_resp['data']['results'].each do |result|
              unless options[:broad_mode].count > 0
                printf "[%10s] Character name: %s\n",
                  result['resourceURI'].split('/').last,
                  result['name']
                unless options[:short] == true
                  printf "%sComics: %-15s Series: %-14s Stories: %-13s\n", ' ' * 13,
                    result['comics']['available'], result['series']['available'], result['stories']['available']
                  printf "%s\n", ' ' * 13 + '-' * 67
                  printf "%sBiography:\n%s\n", ' ' * 13, wrap_text(result['description'], 80, 13)
                  printf "%s\n", ' ' * 13 + '-' * 67
                  printf "%sResource : %s\n", ' ' * 13, MarvelAPI.path_for_url(result['urls'])
                  printf "%sThumbnail: %s\n\n", ' ' * 13, MarvelAPI.path_for_image(result['thumbnail'])
                end
              end
              options[:broad_mode].each do |mode|
                fetch_for_broad_mode(mode, options[:scope_mode], targets, marvel_api, options[:offset], options[:limit_number])
              end
            end
          end
        rescue StandardError => err
          handle_error(err)
        end
      when :comic
        begin
          targets.each do |target|
            filter = {}
            unless target.to_i > 0
              filter[:titleStartsWith] = target
            end
            json_resp = marvel_api.get_comics(options[:limit_number], target, filter)
            if options.has_key? :debug_flag
              puts JSON.pretty_generate(json_resp)
            end
            json_resp['data']['results'].each do |result|
              unless options[:broad_mode].count > 0
                printf "[%10s] Comic title: %s\n",
                  result['resourceURI'].split('/').last,
                  result['title']
                unless options[:short] == true
                  printf "%sIssue number: %-9.6s Format: %-20.20s\n", ' ' * 13,
                    result['issueNumber'],
                    result['format']
                  printf "%sFOC Date: %-13.12s Onsale: %-13.12s Unlimited: %-13.12s\n", ' ' * 13,
                    MarvelAPI.text_for_comic_date(result['dates'], 'foc'),
                    MarvelAPI.text_for_comic_date(result['dates'], 'onsale'),
                    MarvelAPI.text_for_comic_date(result['dates'], 'unlimited')
                  printf "%s\n", ' ' * 13 + '-' * 67
                  printf "%sResource : %s\n", ' ' * 13, MarvelAPI.path_for_url(result['urls'])
                  printf "%sThumbnail: %s\n\n", ' ' * 13, MarvelAPI.path_for_image(result['thumbnail'])
                end
              end
              options[:broad_mode].each do |mode|
                fetch_for_broad_mode(mode, options[:scope_mode], targets, marvel_api, options[:offset], options[:limit_number])
              end
            end
          end
        rescue StandardError => err
          handle_error(err)
        end
      when :creator
        begin
          targets.each do |target|
            filter = {}
            unless target.to_i > 0
              filter[:nameStartsWith] = target
            end
            json_resp = marvel_api.get_creators(options[:limit_number], target, filter)
            if options.has_key? :debug_flag
              puts JSON.pretty_generate(json_resp)
            end
            json_resp['data']['results'].each do |result|
              unless options[:broad_mode].count > 0
                printf "[%10s] Creator name: %s\n",
                  result['resourceURI'].split('/').last,
                  result['fullName']
                unless options[:short] == true
                  printf "%sComics: %-15s Series: %-14s Stories: %-13s\n", ' ' * 13,
                    result['comics']['available'], result['series']['available'], result['stories']['available']
                  printf "%s\n", ' ' * 13 + '-' * 67
                  printf "%sResource : %s\n", ' ' * 13, MarvelAPI.path_for_url(result['urls'])
                  printf "%sThumbnail: %s\n\n", ' ' * 13, MarvelAPI.path_for_image(result['thumbnail'])
                end
              end
              options[:broad_mode].each do |mode|
                fetch_for_broad_mode(mode, options[:scope_mode], targets, marvel_api, options[:offset], options[:limit_number])
              end
            end
          end
        rescue StandardError => err
          handle_error(err)
        end
      when :story
        begin
          targets.each do |target|
            json_resp = marvel_api.get_stories(options[:limit_number], target)
            if options.has_key? :debug_flag
              puts JSON.pretty_generate(json_resp)
            end
            json_resp['data']['results'].each do |result|
              unless options[:broad_mode].count > 0
                printf "[%10s] Story title: %s\n",
                  result['resourceURI'].split('/').last,
                  result['title']
               unless options[:short] == true
                  printf "%sComics: %-15s Series: %-14s Characters: %-13s\n", ' ' * 13,
                    result['comics']['available'], result['series']['available'], result['characters']['available']
                  printf "%s\n", ' ' * 13 + '-' * 67

                  printf "%sCharacters: %s\n",
                    ' ' * 13,
                    result['characters']['items'].count > 0 ? "" : "none listed\n"
                  result['characters']['items'].each do |character|
                    printf "%s[%10s] %s\n", ' ' * 13,
                      character['resourceURI'].split('/').last,
                      character['name']
                    if character.equal?(result['characters']['items'].last)
                      printf "\n"
                    end
                  end

                  printf "%sComics: %s\n",
                    ' ' * 13,
                    result['comics']['items'].count > 0 ? "" : "none listed\n"
                  result['comics']['items'].each do |comic|
                    printf "%s[%10s] %s\n", ' ' * 13,
                      comic['resourceURI'].split('/').last,
                      comic['name']
                    if comic.equal?(result['comics']['items'].last)
                      printf "\n"
                    end
                  end
                end
              end
              options[:broad_mode].each do |mode|
                fetch_for_broad_mode(mode, options[:scope_mode], targets, marvel_api, options[:offset], options[:limit_number])
              end
            end
          end
        rescue StandardError => err
          handle_error(err)
        end
      else
        # handle broad_mode (no scope)
        filter = { "offset": options[:offset] }
        case options[:broad_mode].first
        when :characters
          begin
            filter[:orderBy] = "name"
            json_resp = marvel_api.get_characters(options[:limit_number], "", filter)
            printf "Available: %s\n", json_resp['data']['total']
            output_for_broad_mode(json_resp['data']['results'], 'name')
          rescue StandardError => err
            handle_error(err)
          end
        when :comics
          begin
            filter[:orderBy] = "title"
            json_resp = marvel_api.get_comics(options[:limit_number], "", filter)
            printf "Available: %s\n", json_resp['data']['total']
            output_for_broad_mode(json_resp['data']['results'], 'title')
          rescue StandardError => err
            handle_error(err)
          end
        when :creators
          begin
            filter[:orderBy] = "firstName"
            json_resp = marvel_api.get_creators(options[:limit_number], "", filter)
            printf "Available: %s\n", json_resp['data']['total']
            output_for_broad_mode(json_resp['data']['results'], 'fullName')
          rescue StandardError => err
            handle_error(err)
          end
        when :storys
          begin
            filter[:orderBy] = "id"
            json_resp = marvel_api.get_stories(options[:limit_number], "", filter)
            printf "Available: %s\n", json_resp['data']['total']
            output_for_broad_mode(json_resp['data']['results'], 'title')
          rescue StandardError => err
            handle_error(err)
          end
        end
      end

    end

    private
      def parse_options(argv)
        options = { broad_mode: [] }
        scope_mode_count = 0

        @option_parser = OptionParser.new do |opts|
          opts.banner = "" \
            + "\nUsage: #{@@appname} --ScopeMode [ --BroadMode, ... ] id ..." \
            + "\n       #{@@appname} --BroadMode [ --limit number ]" \
            + "\n" \
            + "\nScopeMode (search by string):" \
            + "\n  #{@@appname} --character name ..." \
            + "\n  #{@@appname} --comic title ..." \
            + "\n  #{@@appname} --creator name ..." \
            + "\n" \
            + "\nScopeMode (search by id):" \
            + "\n  #{@@appname} --character [ --comics | --storys ] id ..." \
            + "\n  #{@@appname} --comic [ --characters | --creators | --storys ] id ..." \
            + "\n  #{@@appname} --creator [ --characters | --comics | --storys ] id ..." \
            + "\n  #{@@appname} --story [ --characters | --comics | --creators ] id ..." \
            + "\n" \
            + "\nBroadMode:" \
            + "\n  #{@@appname} [--characters | --comics | --creators | --storys ] [--limit number]" \
            + "\n" \
            + "\nIn ScopeMode, when targetting by name or title, a BroadMode cannot be" \
            + "\nspecified as well. This is a limitation in the Marvel API. Name and" \
            + "\ntitle searches are always perfomed as a \"starts with\" pattern." \
            + "\n" \
            + "\nIn BroadMode, a maximum of 100 records will be returned at a time." \
            + "\n" \
            + "\nParameters:" \
            + "\n"

          opts.on('--character', 'Scope results to character(s) with id(s)') do
            options[:scope_mode] = :character
            scope_mode_count += 1
          end

          opts.on('--comic', 'Scope results to comic(s) with id(s)') do
            options[:scope_mode] = :comic
            scope_mode_count += 1
          end

          opts.on('--creator', 'Scope results to creator(s) with id(s)') do
            options[:scope_mode] = :creator
            scope_mode_count += 1
          end

          opts.on('--story', 'Scope results to story(s) with id(s)') do
            options[:scope_mode] = :story
            scope_mode_count += 1
          end

          opts.on('--characters', 'List characters') do
            options[:broad_mode].push :characters
          end

          opts.on('--comics', 'List comics') do
            options[:broad_mode].push :comics
          end

          opts.on('--creators', 'List creators') do
            options[:broad_mode].push :creators
          end

          opts.on('--storys', 'List stories') do
            options[:broad_mode].push :storys
          end

          opts.on('-s', '--short', 'Output single line results') do
            options[:short] = true
          end

          opts.on('-l', '--limit number', 'Limit results to number') do |limit|
            # make sure our value isn't another option!
            if limit.start_with? '-'
              raise OptionParser::MissingArgument
            end
            options[:limit_number] = limit.to_i
          end

          opts.on('-o', '--offset number', 'Begin ouput from offset') do |offset|
            # make sure our value isn't another option!
            if offset.start_with? '-'
              raise OptionParser::MissingArgument
            end
            options[:offset] = offset.to_i
          end

          opts.on('--public-key string', 'API public key string') do |key|
            # make sure our value isn't another option!
            if key.start_with? '-'
              raise OptionParser::MissingArgument
            end
            options[:public_key] = key
          end

          opts.on('--private-key string', 'API private key string') do |key|
            # make sure our value isn't another option!
            if key.start_with? '-'
              raise OptionParser::MissingArgument
            end
            options[:private_key] = key
          end

          opts.on('-d', '--debug', 'Enable debugging') do
            options[:debug_flag] = true
          end

          opts.on('-h', '--help', 'Output this help information') do
            puts opts; exit
          end
        end

        begin
          @option_parser.parse!(ARGV)
        rescue OptionParser::MissingArgument => err
          handle_error(err, true)
        rescue OptionParser::ParseError => err
          handle_error(err)
        end

        # grab remaining arguments (one or more targets for scope mode)
        targets = option_parser.parse(ARGV)
        targets = targets.map { |arg| arg.split(',') }.flatten
        targets ||= []

        #
        # sanitize options
        #
        unless (0..1).include? scope_mode_count
          puts option_parser
          exit
        end

        # only one broad_mode if scope_mode is absent
        if options[:broad_mode].count > 1 and scope_mode_count == 0
          puts option_parser
          exit
        end

        # targets are invalid for broad_mode
        if (targets.count > 0 and scope_mode_count == 0) or
          (targets.count == 0 and scope_mode_count > 0)
          puts option_parser
          abort
        end

        # not all broad_mode are valid for scope_mode
        case options[:scope_mode]
        when :character
          if options[:broad_mode].include? :characters or options[:broad_mode].include? :creators
            raise ArgumentError, "invalid arguments"
          end
        when :comic
          if options[:broad_mode].include? :comics
            raise ArgumentError, "invalid arguments"
          end
        when :creator
          if options[:broad_mode].include? :creators or options[:broad_mode].include? :characters
            raise ArgumentError, "invalid arguments"
          end
        when :story
          if options[:broad_mode].include? :storys
            raise ArgumentError, "invalid argument"
          end
        end

        #
        # key file
        #
        # Try and read the key file only if keys not specified on cli. The cli param
        # values override the file param values.
        #
        unless options.has_key?(:public_key) and options.has_key?(:private_key)
          api_keys = load_keys

          unless options.has_key?(:public_key)
            options[:public_key] = api_keys[:public_key]
          end
          unless options.has_key?(:private_key)
            options[:private_key] = api_keys[:private_key]
          end
        end

        # default settings (where needed)
        options[:limit_number] ||= 1
        options[:offset] ||= 0
        options[:short] ||= false

        # debug
        if options.has_key? :debug_flag
          puts "public key: #{options[:public_key]}"
          puts "private key: #{options[:private_key]}"
        end

        [options, targets]
      end

      def load_config(path)
        hash_with_symbols = {}
        expanded_path = File.expand_path(path, File.dirname(__FILE__))
        if File.file?(expanded_path)
          file = File.read(expanded_path)
          file_hash = JSON.parse(file)
          file_hash.keys.each do |key|
            hash_with_symbols[key.to_sym] = file_hash[key]
          end
        end

        return hash_with_symbols
      end

      def load_keys()
        api_keys = load_config('../marvel_keys.json')

        unless api_keys.has_key?(:private_key) and api_keys.has_key?(:public_key)
          raise ArgumentError, "invalid key configuration"
        end

        return api_keys
      end

      def wrap_text(text, width=80, indent=0)
        lines = []
        line = ""

        text.split(/\s+/).each do |word|
          if line.size + word.size >= width
            lines << line
            line = ' ' * indent + word
          elsif line.empty?
           line = ' ' * indent + word
          else
           line << ' ' << word
          end
        end
        lines << line if line

        lines.join "\n"
      end

      def output_for_broad_mode(hash_response, description_key)
        hash_response.each do |object|
          printf "[%10s] %s\n", object['resourceURI'].split('/').last, object[description_key]
        end
      end

      def fetch_for_broad_mode(broad_mode, scope_mode, targets, api, offset=0, limit=1)
        filter = { "#{scope_mode.to_s}s": targets, "offset": offset }
        case broad_mode
        when :characters
          filter[:orderBy] = "name"
          json_resp = api.get_characters(limit, "", filter)
          output_for_broad_mode(json_resp['data']['results'], 'name')
        when :comics
          filter[:orderBy] = "title"
          json_resp = api.get_comics(limit, "", filter)
          output_for_broad_mode(json_resp['data']['results'], 'title')
        when :creators
          filter[:orderBy] = "firstName"
          json_resp = api.get_creators(limit, "", filter)
          output_for_broad_mode(json_resp['data']['results'], 'fullName')
        when :storys
          filter[:orderBy] = "id"
          json_resp = api.get_stories(limit, "", filter)
          output_for_broad_mode(json_resp['data']['results'], 'title')
        end
      end

  end
end
